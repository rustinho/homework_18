#include <iostream>
#include <string>

using namespace std;

template <typename T>
class Stack {
    T* values; // �������� �����
    int top; // ������� ������ �����
    int capacity; // ������������ ������ �����
public:
    Stack(); // ����������� �� ���������

    void Print() const; // ����� ����� �� �����
    void Push(const T& value); // ���������� ��������
    T Pop(); // ���������� ��������

    bool IsEmpty() const; // �������� �� �������

    ~Stack(); // ����������, ������������ ������
};

// ����������� �� ���������
template <typename T>
Stack<T>::Stack() {
    values = new T[1]; // �������� ������ ��� ���� �������
    top = 0; // ������� ����� ��������� �� ������ �������
    capacity = 1; // ���������� �������� ���� �������
}

// ����� ����� �� �����
template <typename T>
void Stack<T>::Print() const {
    if (top == 0) { // ���� ���� ����
        cout << "Stack is empty" << endl; // �������� �� ����
        return; // � �������
    }

    for (int i = top - 1; i >= 0; i--)
        cout << values[i] << " "; // ������� ������ �������

    cout << endl; // ������� ������� �� ����� ������
}

// ���������� �������� � ����
template <typename T>
void Stack<T>::Push(const T& value) {
    values[top++] = value; // ��������� ��������

    if (top < capacity) // ���� �� ����� ���������������� ������
        return; // �������

    capacity *= 2; // ����������� ������� � 2 ����
    T* tmp = new T[capacity]; // �������� ������ ��� ����� ������ � ������� ��������

    for (int i = 0; i < top; i++)
        tmp[i] = values[i]; // �������� ������ ������� � ����� ������

    delete[] values; // ����������� ������ ��-��� �������� �������
    values = tmp; // ��������� ��������� �� ������
}

// ���������� �������� �� �����
template <typename T>
T Stack<T>::Pop() {
    if (IsEmpty()) throw "Stack is empty";
    return values[top > 0]; // ���������� ��������
}

// �������� ����� �� �������
template <typename T>
bool Stack<T>::IsEmpty() const {
    return top == 0; // ����, ���� ���� ���������
}

// ����������, ������������ ������
template <typename T>
Stack<T>::~Stack() {
    delete[] values; // ����������� ������
}

template <typename T>
void MenuStack() {
    Stack<T> stack; // ������ ����
    T value; // �������� ��� �����
    int item; // ����������� ����� ����

    do {
        cout << endl << "Select item of menu" << endl;
        cout << "1. Print" << endl;
        cout << "2. Push" << endl;
        cout << "3. Pop" << endl;
        cout << "4. Quit" << endl;
        cout << ">"; // ������� ����������� �� �����
        cin >> item; // ��������� ����� ����

        // ���� ������������ ����
        while (item < 1 || item > 4) {
            cout << "Invalid item, try again: ";
            cin >> item; // ��������� ����� ����
        }

        // ������������ ��������� ����� ����
        switch (item) {
        case 1: // ������������ ����� ���� "print"
            stack.Print(); // ������� ����
            break;

        case 2: // ������������ ����� ���� "push"
            cout << "Enter value: ";
            cin >> value; // ��������� ��������
            stack.Push(value); // ��������� ������� � ����
            break;

        case 3: // ������������ ����� ���� "pop"
            if (stack.IsEmpty()) { // ���� ���� ����
                cout << "Stack is empty" << endl; // �������� �� ����
            }
            else {
                cout << stack.Pop() << endl; // ������� ������� �� �����
            }
            break;
        }
    } while (item != 4); // ���������, ���� �� ����� �����
}

int main() {
    int type;
    cout << "Select type of stack (1 - int, 2 - double, 3 - string): ";
    cin >> type; // ��������� ���

    if (type == 1) {
        MenuStack<int>();
    }
    else if (type == 2) {
        MenuStack<double>();
    }
    else if (type == 3) {
        MenuStack<string>();
    }
    else {
        cout << "Invalid type..." << endl;
    }

}